import { SecondService } from './second.service';
import { Injectable, forwardRef, Inject } from '@nestjs/common';

@Injectable()
export class AppService {
  constructor(
    @Inject(forwardRef(() => SecondService))
    private secondService: SecondService,
  ) {}

  getHello(): string {
    console.log('this is get hello');
    const servTwo= this.secondService.getHelloAgain();
    console.log(servTwo);
    return 'this is app service';
  }
}

// circular dependencies in both providers and modules
