import { AppService } from './app.service';
import { Injectable, Inject, forwardRef } from '@nestjs/common';

@Injectable()
export class SecondService {


    constructor(@Inject(forwardRef(()=>AppService)) private appService:AppService){}
  getHelloAgain(): string {

    console.log('this is get hello again');
    const serv =this.appService.getHello();
    console.log(serv);
    return 'circular dependent';
  }
}
