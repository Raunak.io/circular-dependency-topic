import { AppModule } from './app.module';
import { SecondService } from './second.service';
import { Module, forwardRef } from '@nestjs/common';
import { AppController } from './app.controller';


@Module({
  imports: [forwardRef(()=>AppModule)],
  controllers: [AppController],
  providers: [SecondService],
  exports: [SecondService],
})
export class NewModule {}